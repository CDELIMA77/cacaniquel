package com.company;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        Apostador apostador = new Apostador();
        ArrayList<Slots> listaSorteios = new ArrayList<>();

        // Se precisar aumentar a qtde de slots, basta ajustar abaixo
        int qtdSlots = 3;

        // Temos que sortear q quantidade de slots parametrizada
        for (int n = 0; n < qtdSlots; n++) {
            listaSorteios.add(apostador.rodarCacaNiquel()) ;
        }

        int totalPontos;
        Calculadora calc = new Calculadora();
        totalPontos = calc.calcularPontos(listaSorteios, qtdSlots);

        Impressora.imprimirLista(listaSorteios, totalPontos);

    }
}