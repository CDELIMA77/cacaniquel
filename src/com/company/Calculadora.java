package com.company;

import java.util.ArrayList;

public class Calculadora {

    public int calcularPontos(ArrayList<Slots> lista, int qtdSlots) {
        int totalPontos = 0;
        int valor1 = 0;
        valor1 = ((int) lista.get(0).getValor());

        for (int i = 0; i < lista.size(); i++) {
            totalPontos = totalPontos + ((int) lista.get(i).getValor());
        }

       //Se todas faces iguais, multiplica por 100
        if (totalPontos == (valor1 * qtdSlots) ) {
            totalPontos = totalPontos * 100;
        }

        return totalPontos;
    }
}